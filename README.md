# Retrofit Android using PHP and MySQL


### I will learn 
- Building REST APIs using SLIM 3 PHP Framework. 
- Making the API Calls from android Side using the Retrofit Library. 
- Deploying the REST API or our backend in a live server. 
- User Authentication


[I used this tutorial to build a project](https://www.youtube.com/playlist?list=PLk7v1Z2rk4hhGfJw-IQCm6kjywmuJX4Rh)
